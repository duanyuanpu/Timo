package com.linln.common.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author  <auntvt＠163.com>
 * @date 2021/3/19
 */
@ComponentScan(basePackages = "com.linln.common")
public class CommonAutoConfig {
}
