package com.linln.modules.memberConfig.service;

import com.linln.common.enums.StatusEnum;
import com.linln.modules.memberConfig.domain.MemberConfig;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/15
 */
public interface MemberConfigService {

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    Page<MemberConfig> getPageList(Example<MemberConfig> example);

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    MemberConfig getById(Long id);

    /**
     * 保存数据
     * @param memberConfig 实体对象
     */
    MemberConfig save(MemberConfig memberConfig);

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Transactional(rollbackFor = Exception.class)
    Boolean updateStatus(StatusEnum statusEnum, List<Long> idList);
}