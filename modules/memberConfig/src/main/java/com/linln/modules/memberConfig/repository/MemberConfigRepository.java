package com.linln.modules.memberConfig.repository;

import com.linln.modules.memberConfig.domain.MemberConfig;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author dyp
 * @date 2021/09/15
 */
public interface MemberConfigRepository extends BaseRepository<MemberConfig, Long> {
}