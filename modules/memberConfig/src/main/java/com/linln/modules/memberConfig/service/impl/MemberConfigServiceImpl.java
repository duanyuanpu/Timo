package com.linln.modules.memberConfig.service.impl;

import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import com.linln.modules.memberConfig.domain.MemberConfig;
import com.linln.modules.memberConfig.repository.MemberConfigRepository;
import com.linln.modules.memberConfig.service.MemberConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/15
 */
@Service
public class MemberConfigServiceImpl implements MemberConfigService {

    @Autowired
    private MemberConfigRepository memberConfigRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    public MemberConfig getById(Long id) {
        return memberConfigRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<MemberConfig> getPageList(Example<MemberConfig> example) {
        // 创建分页对象
    	PageRequest page = PageSort.pageRequest(Sort.Direction.ASC);
        return memberConfigRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param memberConfig 实体对象
     */
    @Override
    public MemberConfig save(MemberConfig memberConfig) {
        return memberConfigRepository.save(memberConfig);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return memberConfigRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}