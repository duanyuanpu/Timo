package com.linln.modules.memberConfig.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages = "com.linln.modules.memberConfig")
@EnableJpaRepositories(basePackages = "com.linln.modules.memberConfig")
@EntityScan(basePackages = "com.linln.modules.memberConfig")
public class AutoConfig {
}
