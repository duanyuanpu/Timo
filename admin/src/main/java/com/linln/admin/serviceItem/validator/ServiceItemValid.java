package com.linln.admin.serviceItem.validator;

import lombok.Data;

import java.io.Serializable;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author dyp
 * @date 2021/09/17
 */
@Data
public class ServiceItemValid implements Serializable {
    @NotEmpty(message = "名称不能为空")
    private String name;
    @NotEmpty(message = "类型不能为空")
    private String type;
    @NotNull(message = "价格不能为空")
    @Pattern(regexp = "(^(([1-9]{1}\\d*)|(0{1}))(\\.\\d{1,2})?$)", message = "价格格式不正确")
    private String price;
}