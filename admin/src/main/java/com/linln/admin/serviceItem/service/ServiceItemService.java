package com.linln.admin.serviceItem.service;

import com.linln.admin.serviceItem.domain.ServiceItem;
import com.linln.common.enums.StatusEnum;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/17
 */
public interface ServiceItemService {

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    Page<ServiceItem> getPageList(Example<ServiceItem> example);

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    ServiceItem getById(Long id);

    /**
     * 保存数据
     * @param serviceItem 实体对象
     */
    ServiceItem save(ServiceItem serviceItem);

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Transactional(rollbackFor = Exception.class)
    Boolean updateStatus(StatusEnum statusEnum, List<Long> idList);
}