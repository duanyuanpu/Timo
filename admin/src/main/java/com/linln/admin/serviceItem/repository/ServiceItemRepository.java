package com.linln.admin.serviceItem.repository;

import com.linln.admin.serviceItem.domain.ServiceItem;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author dyp
 * @date 2021/09/17
 */
public interface ServiceItemRepository extends BaseRepository<ServiceItem, Long> {
}