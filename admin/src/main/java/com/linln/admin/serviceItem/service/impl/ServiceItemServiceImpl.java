package com.linln.admin.serviceItem.service.impl;

import com.linln.admin.serviceItem.domain.ServiceItem;
import com.linln.admin.serviceItem.repository.ServiceItemRepository;
import com.linln.admin.serviceItem.service.ServiceItemService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/17
 */
@Service
public class ServiceItemServiceImpl implements ServiceItemService {

    @Autowired
    private ServiceItemRepository serviceItemRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    public ServiceItem getById(Long id) {
        return serviceItemRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<ServiceItem> getPageList(Example<ServiceItem> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest(Sort.Direction.ASC);
        return serviceItemRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param serviceItem 实体对象
     */
    @Override
    public ServiceItem save(ServiceItem serviceItem) {
        return serviceItemRepository.save(serviceItem);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return serviceItemRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}