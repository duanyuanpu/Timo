package com.linln.admin.serviceItem.controller;

import com.linln.admin.serviceItem.domain.ServiceItem;
import com.linln.admin.serviceItem.service.ServiceItemService;
import com.linln.admin.serviceItem.validator.ServiceItemValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/17
 */
@Controller
@RequestMapping("/serviceItem")
public class ServiceItemController {

    @Autowired
    private ServiceItemService serviceItemService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("serviceItem:index")
    public String index(Model model, ServiceItem serviceItem) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", match -> match.contains());

        // 获取数据列表
        Example<ServiceItem> example = Example.of(serviceItem, matcher);
        Page<ServiceItem> list = serviceItemService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/serviceItem/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("serviceItem:add")
    public String toAdd() {
        return "/serviceItem/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("serviceItem:edit")
    public String toEdit(@PathVariable("id") ServiceItem serviceItem, Model model) {
        model.addAttribute("serviceItem", serviceItem);
        return "/serviceItem/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"serviceItem:add", "serviceItem:edit"})
    @ResponseBody
    public ResultVo save(@Validated ServiceItemValid valid, ServiceItem serviceItem) {
        // 复制保留无需修改的数据
        if (serviceItem.getId() != null) {
            ServiceItem beServiceItem = serviceItemService.getById(serviceItem.getId());
            EntityBeanUtil.copyProperties(beServiceItem, serviceItem);
        }

        // 保存数据
        serviceItemService.save(serviceItem);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("serviceItem:detail")
    public String toDetail(@PathVariable("id") ServiceItem serviceItem, Model model) {
        model.addAttribute("serviceItem",serviceItem);
        return "/serviceItem/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("serviceItem:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (serviceItemService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}