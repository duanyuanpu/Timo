package com.linln.admin.member.service.impl;

import com.linln.admin.member.domain.Member;
import com.linln.admin.member.repository.MemberRepository;
import com.linln.admin.member.service.MemberService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/06
 */
@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberRepository memberRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    public Member getById(Long id) {
        return memberRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<Member> getPageList(Example<Member> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest(Sort.Direction.ASC);
        return memberRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param member 实体对象
     */
    @Override
    public Member save(Member member) {
        return memberRepository.save(member);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return memberRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }

	@Override
	public boolean checkPhone(Member member) {
		boolean flag = false;
		List<Member> list = memberRepository.findPhoneMember(member);
		if(list!=null && list.size()>0) {
			flag = true;
		}
		return flag;
	}
}