package com.linln.admin.member.controller;

import com.linln.admin.member.domain.Member;
import com.linln.admin.member.service.MemberService;
import com.linln.admin.member.validator.MemberValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/06
 */
@Controller
@RequestMapping("/member/member")
public class MemberController {

    @Autowired
    private MemberService memberService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("member:member:index")
    public String index(Model model, Member member) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", match -> match.contains())
                .withMatcher("phone", match -> match.contains())
                .withMatcher("remark", match -> match.contains());

        // 获取数据列表
        Example<Member> example = Example.of(member, matcher);
        Page<Member> list = memberService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/member/member/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("member:member:add")
    public String toAdd() {
        return "/member/member/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("member:member:edit")
    public String toEdit(@PathVariable("id") Member member, Model model) {
        model.addAttribute("member", member);
        return "/member/member/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"member:member:add", "member:member:edit"})
    @ResponseBody
    public ResultVo save(@Validated MemberValid valid, Member member) {
        // 复制保留无需修改的数据
        if (member.getId() != null) {
            Member beMember = memberService.getById(member.getId());
            EntityBeanUtil.copyProperties(beMember, member);
        }
        
        //查询手机号是否重复
        if(memberService.checkPhone(member)) {
        	return ResultVoUtil.error("已经存在相同手机号的会员！");
        }

        // 保存数据
        memberService.save(member);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("member:member:detail")
    public String toDetail(@PathVariable("id") Member member, Model model) {
        model.addAttribute("member",member);
        return "/member/member/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("member:member:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (memberService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}