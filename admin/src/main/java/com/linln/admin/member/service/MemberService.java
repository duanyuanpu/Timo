package com.linln.admin.member.service;

import com.linln.admin.member.domain.Member;
import com.linln.common.enums.StatusEnum;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/06
 */
public interface MemberService {

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    Page<Member> getPageList(Example<Member> example);

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    Member getById(Long id);

    /**
     * 保存数据
     * @param member 实体对象
     */
    Member save(Member member);

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Transactional(rollbackFor = Exception.class)
    Boolean updateStatus(StatusEnum statusEnum, List<Long> idList);

	boolean checkPhone(Member member);
}