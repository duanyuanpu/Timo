package com.linln.admin.member.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.linln.admin.member.domain.Member;
import com.linln.common.constant.StatusConst;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author dyp
 * @date 2021/09/06
 */
public interface MemberRepository extends BaseRepository<Member, Long> {
	
	@Query(nativeQuery = true,value = "select * from or_member m where m.status="+StatusConst.OK+" and m.phone=:#{#member.phone} and if(:#{#member.id}!='',m.id!=:#{#member.id},1=1)")
	List<Member> findPhoneMember(@Param("member")Member member);
}