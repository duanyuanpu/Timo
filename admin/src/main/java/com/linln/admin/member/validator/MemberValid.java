package com.linln.admin.member.validator;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author dyp
 * @date 2021/09/06
 */
@Data
public class MemberValid implements Serializable {
    @NotEmpty(message = "姓名不能为空")
    private String name;
    @Pattern(regexp = "^((17[0-9])|(14[0-9])|(13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$", message = "手机号码格式不正确")
    @NotEmpty(message = "电话不能为空")
    private String phone;
    @NotNull(message = "生日不能为空")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date birthday;
    @Pattern(regexp = "(^(([1-9]{1}\\d*)|(0{1}))(\\.\\d{1,2})?$)", message = "账户余额格式不正确")
    private String account;
}