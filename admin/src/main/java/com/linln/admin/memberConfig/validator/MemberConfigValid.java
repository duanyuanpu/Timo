package com.linln.admin.memberConfig.validator;

import lombok.Data;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * @author dyp
 * @date 2021/09/15
 */
@Data
public class MemberConfigValid implements Serializable {
    @NotEmpty(message = "级别不能为空")
    private String level;
    
    @NotEmpty(message = "充值金额不能为空")
    @Pattern(regexp = "(^(([1-9]{1}\\d*)|(0{1}))(\\.\\d{1,2})?$)", message = "充值金额格式不正确")
    private String rechargeAmount;
    
    @NotEmpty(message = "折扣不能为空")
    @Pattern(regexp = "(^(([1-9]{1}\\d*)|(0{1}))(\\.\\d{1,2})?$)", message = "折扣格式不正确")
    private String discount;
}