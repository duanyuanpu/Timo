package com.linln.admin.memberConfig.controller;

import com.linln.admin.memberConfig.validator.MemberConfigValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import com.linln.modules.memberConfig.domain.MemberConfig;
import com.linln.modules.memberConfig.service.MemberConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/15
 */
@Controller
@RequestMapping("/memberConfig")
public class MemberConfigController {

    @Autowired
    private MemberConfigService memberConfigService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("memberConfig:index")
    public String index(Model model, MemberConfig memberConfig) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("level", match -> match.contains());

        // 获取数据列表
        Example<MemberConfig> example = Example.of(memberConfig, matcher);
        Page<MemberConfig> list = memberConfigService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/memberConfig/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("memberConfig:add")
    public String toAdd() {
        return "/memberConfig/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("memberConfig:edit")
    public String toEdit(@PathVariable("id") MemberConfig memberConfig, Model model) {
        model.addAttribute("memberConfig", memberConfig);
        return "/memberConfig/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"memberConfig:add", "memberConfig:edit"})
    @ResponseBody
    public ResultVo save(@Validated MemberConfigValid valid, MemberConfig memberConfig) {
        // 复制保留无需修改的数据
        if (memberConfig.getId() != null) {
            MemberConfig beMemberConfig = memberConfigService.getById(memberConfig.getId());
            EntityBeanUtil.copyProperties(beMemberConfig, memberConfig);
        }

        // 保存数据
        memberConfigService.save(memberConfig);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("memberConfig:detail")
    public String toDetail(@PathVariable("id") MemberConfig memberConfig, Model model) {
        model.addAttribute("memberConfig",memberConfig);
        return "/memberConfig/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("memberConfig:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (memberConfigService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}