package com.linln.admin.actCustom.validator;

import lombok.Data;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;

/**
 * @author dyp
 * @date 2021/09/23
 */
@Data
public class ActCustomValid implements Serializable {
    @NotEmpty(message = "名称不能为空")
    private String name;
    @NotEmpty(message = "类型不能为空")
    private String type;
}