package com.linln.admin.actCustom.service.impl;

import com.linln.admin.actCustom.domain.ActCustom;
import com.linln.admin.actCustom.repository.ActCustomRepository;
import com.linln.admin.actCustom.service.ActCustomService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/23
 */
@Service
public class ActCustomServiceImpl implements ActCustomService {

    @Autowired
    private ActCustomRepository actCustomRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    public ActCustom getById(Long id) {
        return actCustomRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<ActCustom> getPageList(Example<ActCustom> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return actCustomRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param actCustom 实体对象
     */
    @Override
    public ActCustom save(ActCustom actCustom) {
        return actCustomRepository.save(actCustom);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return actCustomRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}