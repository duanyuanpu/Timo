package com.linln.admin.actCustom.repository;

import com.linln.admin.actCustom.domain.ActCustom;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author dyp
 * @date 2021/09/23
 */
public interface ActCustomRepository extends BaseRepository<ActCustom, Long> {
}