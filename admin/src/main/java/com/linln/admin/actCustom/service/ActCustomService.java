package com.linln.admin.actCustom.service;

import com.linln.admin.actCustom.domain.ActCustom;
import com.linln.common.enums.StatusEnum;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/23
 */
public interface ActCustomService {

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    Page<ActCustom> getPageList(Example<ActCustom> example);

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    ActCustom getById(Long id);

    /**
     * 保存数据
     * @param actCustom 实体对象
     */
    ActCustom save(ActCustom actCustom);

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Transactional(rollbackFor = Exception.class)
    Boolean updateStatus(StatusEnum statusEnum, List<Long> idList);
}