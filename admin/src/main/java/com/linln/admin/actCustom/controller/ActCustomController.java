package com.linln.admin.actCustom.controller;

import com.linln.admin.actCustom.domain.ActCustom;
import com.linln.admin.actCustom.service.ActCustomService;
import com.linln.admin.actCustom.validator.ActCustomValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author dyp
 * @date 2021/09/23
 */
@Controller
@RequestMapping("/actCustom")
public class ActCustomController {

    @Autowired
    private ActCustomService actCustomService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("actCustom:index")
    public String index(Model model, ActCustom actCustom) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", match -> match.contains());

        // 获取数据列表
        Example<ActCustom> example = Example.of(actCustom, matcher);
        Page<ActCustom> list = actCustomService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/actCustom/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("actCustom:add")
    public String toAdd() {
        return "/actCustom/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("actCustom:edit")
    public String toEdit(@PathVariable("id") ActCustom actCustom, Model model) {
        model.addAttribute("actCustom", actCustom);
        return "/actCustom/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"actCustom:add", "actCustom:edit"})
    @ResponseBody
    public ResultVo save(@Validated ActCustomValid valid, ActCustom actCustom) {
        // 复制保留无需修改的数据
        if (actCustom.getId() != null) {
            ActCustom beActCustom = actCustomService.getById(actCustom.getId());
            EntityBeanUtil.copyProperties(beActCustom, actCustom);
        }

        // 保存数据
        actCustomService.save(actCustom);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("actCustom:detail")
    public String toDetail(@PathVariable("id") ActCustom actCustom, Model model) {
        model.addAttribute("actCustom",actCustom);
        return "/actCustom/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("actCustom:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (actCustomService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}