package com.linln.component.thymeleaf.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author  <auntvt＠163.com>
 * @date 2021/3/19
 */
@ComponentScan(basePackages = "com.linln.component.thymeleaf")
public class ThymeleafAutoConfig {
}
